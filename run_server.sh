#!/bin/bash

## TODO: Verify if install_server.sh must run first

echo "To kill server, type 'serverexit 100' and enter, DO NOT CTRL-C, or it will not close totally."
sleep 5

cd server-data
./test_login &
./test_char &
./test_map
