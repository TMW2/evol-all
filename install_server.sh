echo "This is for Ubuntu 22.04 with super user access"
sudo apt-get install -y gcc-9 make autoconf automake autopoint mysql-client=8.0.28-0ubuntu4 libmysqlclient21=8.0.28-0ubuntu4 libssl-dev libtool libmysqlclient-dev=8.0.28-0ubuntu4 libz-dev libpcre3-dev
./init.sh all
cd tools/localserver
./installconfigs.sh
./build.sh # <- THIS SCRIPT will use gcc-9 (see apt above), but it should work up to gcc-12 if you edit this script file (thanks to Test_User)
./initdb.sh
cd ../../server-data
#./.tools/jobs/runserver.sh mysql
echo "Done, Moubootaur Legends Server was installed. Run the server in three terminals, and you can test locally with tools/manaplus/connect_local_server.sh with user admin password admin - keep in mind server will NOT be production-ready on init."

